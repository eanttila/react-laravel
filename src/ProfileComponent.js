import { useHistory , Link, Route, Switch, useRouteMatch } from 'react-router-dom';
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

const ProfileComponent = ({ isLoggedIn }) => {
	const { path } = useRouteMatch();
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token && isLoggedIn) {
			const token = localStorage.getItem("token");
			checkManagement(token);
		} else {
			history.push("/");
		}
	}, [isLoggedIn])
	
	const history = useHistory();
	const [user, setUser] = React.useState(false);
	const [allowManage, setAllowManage] = React.useState(false);
	
	const checkManagement = async (token) => { //not in use in this project !!!!
		try {
			const res = await fetch('api/auth/user-profile', { headers: {"Authorization" : `Bearer ${token}`} });
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			//if (json.subscription === 'admin') {
				setAllowManage(true);
			//}
			setUser(json);
		} catch (error) {
			localStorage.clear();
			//alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
const Manage = ({}) => {
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token) {
			getTasks();
		}
	}, [])
	const [tasks, setTasks] = React.useState([]);
	
	const getTasks = async () => {
		const token = localStorage.getItem("token");
		try {
			const res = await fetch('api/tasks/tasks', { headers: {"Authorization" : `Bearer ${token}`} });
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			setTasks(json);
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
	const Tasks = ({ items }) => {
	React.useEffect(() => {

	}, [items])
	
	  return (
	  <div><h4>My tasks:</h4>
	  		{items.map(task => {
			return(
			<div key={user.id} className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px', boxShadow: '1px 1px 10px #808080'}}>
			<b style={{paddingRight: '5px'}}>Task name:</b> {task.description}<hr/>
			<Link to={`/profile/manage/tasks/${user.id}`}>
			<button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Show</button>
			</Link></div>)})
		}
	  </div>
	)
}

const Task = ({itemId}) => {
	React.useEffect(() => {
		document.documentElement.scrollTop = 0;
		const token = localStorage.getItem("token");
		if (token) {
		fetch('api/tasks/tasks/'+itemId, { headers: {"Authorization" : `Bearer ${token}`} }).then(function (response) {
			return response.json();
		}).then(function (json) {
			setTask(json[0])
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
		}
	}, [itemId])
	
	const [task, setTask] = React.useState(false);
	
	if (!task) {
	  return (
	  <div style={{border: '1px solid lightGray', height: '130px', maxWidth: '800px'}}>
		<img src={'http://eax.arkku.net/loading.gif'} style={{height: '130px', width: '130px', display: 'block', margin: 'auto'}}/>
		</div> 
	  )
	}
	
  return (
  <div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>Id:</b> {task.id}
	</div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>Description:</b> {task.description}
	</div>
	<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
		<b style={{paddingRight: '5px'}}>User id:</b> {task.user_id}
	</div>
  </div>
  )
}
	
return(
  <Switch>
    <Route exact path={['/profile/manage']} render={() => ( <Tasks items={tasks} /> )}/>
	<Route path={["/profile/manage/tasks/:id"]} render={({ match }) => {
		const itemId = match.params.id;
        return <Task itemId={itemId} />
    }} />
  </Switch>
)
}
	
return(
	<>
      <h2>Profile</h2>
  <div>	
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Id:</b> {user.id}
		</div>
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Name:</b> {user.name}
		</div>
		<div className="row" style={{padding: '10px', marginLeft: '0px', border: '1px solid lightGray', maxWidth: '500px'}}>
			<b style={{paddingRight: '5px'}}>Email:</b> {user.email}
		</div>
	  </div><br/>
	  <nav style={{}}>
      <ul style={{display:'block', paddingLeft: '0px'}}>
	  { allowManage ? <Link to='/profile/manage'><li className='list-inline-item' style={{color: '#007bff', minWidth: '100px', margin: '0px', padding: '10px', border: '1px solid lightGray'}}>Show my tasks list</li></Link> : '' }
      </ul>
	</nav>
      <Switch>
        <Route path={`${path}/manage`}>
          <Manage />
        </Route>
      </Switch>
	  <br/><br/><br/>
    </>
)}

export default ProfileComponent;