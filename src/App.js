import React from 'react';
import { HashRouter, Redirect, Link, Route, Switch } from 'react-router-dom';
import {hot} from 'react-hot-loader';
import './app.css';
import PostsComponent from './PostsComponent';
import ProfileComponent from './ProfileComponent';
import TestComponent from './TestComponent';
import RegisterComponent from './RegisterComponent';

function App() {
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token) {
			testJwt(token);
		} else {
			localStorage.clear();
			setIsLoggedIn(false);
		}
	}, [])
	
	const [isLoggedIn, setIsLoggedIn] = React.useState(false);
	const [isLoading, setIsLoading] = React.useState(false);
	const [username, setUsername] = React.useState('testerman@testerman.com'); //tester
	const [password, setPassword] = React.useState('testerman'); //tester
	const [factors, setFactors] = React.useState([]);
	//localhost/react-laravel/api/auth/login
	const logIn = async () => {
		try {
			let formData = new FormData();
			formData.append('email', username);
			formData.append('password', password);		
			setIsLoading(true);
			const res = await fetch('api/auth/login', {
			  method: 'POST',
			  body: formData
			});
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			setIsLoggedIn(true);
			localStorage.setItem("token", JSON.stringify(json.access_token).slice(1, -1));
			setIsLoading(false);
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
			setIsLoading(false);
	  }		
	};
	
	const testJwt = async (token) => {
		try {
			const res = await fetch('api/auth/refresh', {
			method: 'POST',
			headers: {"Authorization" : `Bearer ${token}`} });
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			//console.log(JSON.stringify(json.access_token).slice(1, -1));
			setIsLoggedIn(true);
			localStorage.setItem("token", JSON.stringify(json.access_token).slice(1, -1));
		} catch (error) {
			//alert(`Error fetching or parsing data: ${error}`);
			localStorage.clear();
			setIsLoggedIn(false);
	  }
	};
	
	const logOut = async () => {
		const token = localStorage.getItem("token");
		if (token) {
			try {
				const res = await fetch('api/auth/logout', {
				method: 'POST',
				headers: {"Authorization" : `Bearer ${token}`} });
				if (!res.ok) {
					throw new Error("Fetch was not OK");
				}
				const json = await res.json();
				localStorage.clear();
				setIsLoggedIn(false);
			} catch (error) {
				alert(`Error fetching or parsing data: ${error}`);
		  }
		}
	};
	
	const getProducts = async (areaName) => {
		try {
			const res = await fetch('/tori/'+areaName);
			if (!res.ok) {
				//setItem(null)
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			return json			
		} catch (error) {
			//alert(`Error fetching or parsing data: ${error}`);
	  }
	};
	
  return (
      <div className="wrapper">
	  <div style={{border: '0px solid black', height: '60px', background: 'lavender', backgroundRepeat: 'no-repeat',
	backgroundPosition: '10px', borderRadius: '5px', border: '1px solid lightGray'}}>
	<small style={{margin: '0px', padding: '5px'}}>(Made by EA)</small>
	</div>			
	   <br/>
	   
	   {/*<button type="submit" className="btn btn-info" onClick={profileTest}>profileTest</button>*/}
	   {/*isLoggedIn ? 'isLoggedIn!' : 'Not loggedIn!'*/}<hr/>
	   { !isLoggedIn ?
	   <div>
	   { !isLoading ? <div>
		<input type="text" required={true} className="form-control input-sm" style={{maxHeight: '35px', maxWidth: '350px'}} defaultValue={username} placeholder="username" name="area" onChange={e =>setUsername(e.target.value)}/>
		<input type="text" required={true} className="form-control input-sm" style={{maxHeight: '35px', maxWidth: '350px'}} defaultValue={password} placeholder="password" name="area" onChange={e =>setPassword(e.target.value)}/>
		<button type="submit" className="btn btn-info" onClick={logIn}>Login</button>
		</div> : <div style={{border: '1px solid lightGray', height: '130px', maxWidth: '800px'}}>
		<img src={'http://eax.arkku.net/loading.gif'} style={{height: '130px', width: '130px', display: 'block', margin: 'auto'}}/>
		</div> }</div>
	   : 
		<button type="submit" className="btn btn-info" onClick={logOut}>Logout</button> }
		<hr/>
		
      <h3>Testing</h3>	  
      <HashRouter>
        <nav>
          <ul style={{padding: '0px', display:'block'}}>
		  <div style={{paddingLeft: '15px'}}>
			<Link to="/posts"><li className='list-inline-item' style={{color: '#007bff', minWidth: '100px', margin: '0px', padding: '10px',border: '1px solid lightGray'}}>Posts</li></Link>
			{ isLoggedIn ? <Link to="/profile"><li className='list-inline-item' style={{color: '#007bff', minWidth: '100px', margin: '0px', padding: '10px',border: '1px solid lightGray'}}>Profile</li></Link> : '' }
			{ !isLoggedIn ? <Link to="/register"><li className='list-inline-item' style={{color: '#007bff', minWidth: '100px', margin: '0px', padding: '10px',border: '1px solid lightGray'}}>Register</li></Link> : '' }
			</div>
          </ul>
        </nav>
        <Switch>
		{/*<Route exact path="/">
            { doneLoading ? <Area areaName='Keski-Suomi' items={KsItems} setItems={setKsItems} /> : '' }
		</Route>*/}
		<Route
		path="/posts"
		render={() => (
		<PostsComponent
		items={factors}
		setItems={null}
		/>
		)}/>
		<Route
		path="/profile"
		render={() => (
		<ProfileComponent
		isLoggedIn={isLoggedIn}
		nameProp={'profile'}
		/>
		)}/>
		<Route
		path="/register"
		render={() => (
		<RegisterComponent
		setIsLoggedIn={setIsLoggedIn}
		/>
		)}/>
          {/*<Route path="/main">
            <Main />
          </Route>*/}
        </Switch>
      </HashRouter>
    </div>
  );
}

export default hot(module)(App);