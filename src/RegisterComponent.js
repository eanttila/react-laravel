import { useHistory, Link, Route, Switch } from 'react-router-dom';
import React from 'react';
import * as ReactBootstrap from 'react-bootstrap';

const RegisterComponent = ({ setIsLoggedIn }) => {
	React.useEffect(() => {
		const token = localStorage.getItem("token");
		if (token) { history.push("/profile"); }
	}, [setIsLoggedIn])
	
	const history = useHistory();
	const [isLoading, setIsLoading] = React.useState(false);
	const [name, setName] = React.useState('');
	const [username, setUsername] = React.useState('');
	const [password, setPassword] = React.useState('');
	
	const register = async () => {
		try {
			setIsLoading(true);
			const data = new URLSearchParams();
			data.append('username', username);
			data.append('password', password);
			const res = await fetch('http://localhost/cfd/auth/register', {
			  method: 'POST',
			  headers: {
				'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
			  },
			  body: data
			});
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			if (json.username === username) {
				logIn();
			}
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
	const logIn = async () => {
		try {
			const res = await fetch('http://localhost/cfd/auth/login_check', {
			  method: 'POST',
			  headers: {
				//'Access-Control-Allow-Origin': '*',
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			  },
			  body: JSON.stringify(
				{ username: username, password: password }
				)
			});
			if (!res.ok) {
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();
			localStorage.setItem("token", JSON.stringify(json.token).slice(1, -1));
			setIsLoggedIn(true);
			history.push("/profile");
		} catch (error) {
			alert(`Error fetching or parsing data: ${error}`);
	  }		
	};
	
return(
  <div>
  { !isLoading ?
  <div>
		<input type="text" required={true} className="form-control input-sm" style={{maxHeight: '35px', maxWidth: '350px'}} defaultValue={username} placeholder="username" name="area" onChange={e =>setUsername(e.target.value)}/>
		<input type="text" required={true} className="form-control input-sm" style={{maxHeight: '35px', maxWidth: '350px'}} defaultValue={password} placeholder="password" name="area" onChange={e =>setPassword(e.target.value)}/>
		<button type="submit" className="btn btn-info" onClick={register}>Register</button>
	</div> : 
		<div style={{border: '1px solid lightGray', height: '130px', maxWidth: '800px'}}>
		<img src={'http://eax.arkku.net/loading.gif'} style={{height: '130px', width: '130px', display: 'block', margin: 'auto'}}/>
		</div> }
  	</div>
)}

export default RegisterComponent;